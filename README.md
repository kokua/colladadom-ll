# To Build
autobuild install --platform  Linux, Linux64, windows, darwin

autobuild build --platform

autobuild package --platform

# Example
```
autobuild install --platform linux64
autobuild build   --platform linux64
autobuild package --platform linux64
```


